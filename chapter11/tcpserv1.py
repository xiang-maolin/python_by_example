# import socket
#
# host = ''             # 空字符串表示本机所有地址，即0.0.0.0
# port = 12345         # 端口号是整数类型
# addr = (host, port)
# s = socket.socket()    # socker()方法默认创建TCP套接字
# s.bind(addr)         # 绑定地址到套接字
# s.listen(1)           # 启动侦听过程
# cli_sock, cli_addr = s.accept()   # 用于接受客户机的连接
# print('Client from:', cli_addr)
# data = cli_sock.recv(1024)     # 最多一次接受1024字节的数据
# print(data.decode())
# cli_sock.send(b'Got it.\r\n')
# cli_sock.close()   # 关闭客户机套接字
# s.close()         # 关闭服务器套接字

###############################
# import socket
#
# host = ''
# port = 12345
# addr = (host, port)
# s = socket.socket()
# s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# s.bind(addr)
# s.listen(1)
# cli_sock, cli_addr = s.accept()
# print('Client from:', cli_addr)
#
# while True:
#     data = cli_sock.recv(1024)
#     if data.strip() == b'quit':
#         cli_sock.send(b'Bye-bye!\r\n')
#         break
#     print(data.decode())
#     cli_sock.send(b'Got it.\r\n')
#
# cli_sock.close()
# s.close()

####################################
import socket

host = ''
port = 12345
addr = (host, port)
s = socket.socket()
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(addr)
s.listen(1)

while True:
    cli_sock, cli_addr = s.accept()
    print('Client from:', cli_addr)

    while True:
        data = cli_sock.recv(1024)
        if data.strip() == b'quit':
            cli_sock.send(b'Bye-bye!\r\n')
            break
        print(data.decode())
        cli_sock.send(b'Got it.\r\n')

    cli_sock.close()

s.close()
