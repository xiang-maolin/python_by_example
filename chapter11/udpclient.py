import socket
import sys

def udp_client(serv, port):
    addr = (serv, port)
    client = socket.socket(type=socket.SOCK_DGRAM)

    while True:
        data = input('(quit to exit)> ') + '\r\n'
        if data.strip() == 'quit':
            break

        client.sendto(data.encode(), addr)
        rdata = client.recvfrom(1024)[0]
        print(rdata.decode())

    client.close()

if __name__ == '__main__':
    server = sys.argv[1]
    port = int(sys.argv[2])
    udp_client(server, port)
