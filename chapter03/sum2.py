sum100 = 0
counter = 0

while counter < 100:
    counter += 1    # 注意，每次循环计数器的值都需要有机会增加
    if counter % 2 == 1:
        continue
    else:
        sum100 += counter

print(sum100)
