def outer():
    counter = 0
    def inner():
        nonlocal counter
        print('Inner func')
        counter += 1
        print(counter)

    return inner

if __name__ == '__main__':
    result = outer()
    result()
