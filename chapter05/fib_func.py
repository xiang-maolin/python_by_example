def gen_fib():
    fib = [0, 1]

    length = int(input("数列长度: "))
    for i in range(length - 2):  # 因为fib数列中已有两个初值
        fib.append(fib[-1] + fib[-2])

    print(fib)


gen_fib()  # 注意此处没有缩进
gen_fib()
