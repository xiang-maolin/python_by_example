from dbconn2 import Session, Departments

session = Session()

hr = Departments(dep_id=1, dep_name='人事部')
finance = Departments(dep_id=2, dep_name='财务部')
ops = Departments(dep_id=3, dep_name='运维部')
deps = [hr, finance, ops]
session.add_all(deps)
session.commit()

qset1 = session.query(Departments)
for dep in qset1:
    print('%s: %s' % (dep.dep_id, dep.dep_name))

session.close()
