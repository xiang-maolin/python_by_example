def blocks(fobj):
    block = []      # 保存中间结果的列表
    counter = 0     # 计数器，够10行则返回
    for line in fobj:
        block.append(line)
        counter += 1
        if counter == 10:
            yield block  # 返回中间结果，下次取值，从这里向下执行
            block = []   # 清空列表，以备下次使用
            counter = 0  # 计数器清0
    if block:            # 文件最后不够10行的部分
        yield block

if __name__ == '__main__':
    fobj = open('/etc/passwd')
    for lines in blocks(fobj):
        print(lines)
        print('#' * 50)
    fobj.close()
