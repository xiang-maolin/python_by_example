import re
from collections import Counter

class CountPatt:
    def __init__(self, fname):
        self.fname = fname

    def count_patt(self, patt):
        result = Counter()
        cpatt = re.compile(patt)

        with open(self.fname) as fobj:
            for line in fobj:
                m = cpatt.search(line)
                if m:
                    result.update([m.group()])

        return result

if __name__ == '__main__':
    fname = 'access_log'
    cp = CountPatt(fname)
    ip = '^(\d+\.){3}\d+'
    ip_count = cp.count_patt(ip)
    print(ip_count)
    print('*' * 30)
    print(ip_count.most_common(3))

